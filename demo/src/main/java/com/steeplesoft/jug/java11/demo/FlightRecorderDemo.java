package com.steeplesoft.jug.java11.demo;

import jdk.jfr.Description;
import jdk.jfr.Event;
import jdk.jfr.Label;

import java.io.IOException;

public class FlightRecorderDemo {
    public static void main(String... args) throws IOException {
        int count = 1;
        while (true) {
            HelloWorld event = new HelloWorld();
            event.message = String.format("hello, world! %d", count++);
            event.commit();
        }
    }
}


@Label("Hello World")
@Description("Helps the programmer getting started")
class HelloWorld extends Event {
    @Label("Message")
    String message;
}

