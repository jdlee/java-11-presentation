package com.steeplesoft.jug.java11.demo;

import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class HttpClientDemo {
    public void sync() throws IOException, InterruptedException {
        HttpClient client = createClient();
        HttpRequest request = createRequest();
        HttpResponse<Stream<String>> response = client.send(request,
                HttpResponse.BodyHandlers.ofLines());

        int status = response.statusCode();

        if (status == 200) {
            System.out.println("Response status code: " + response.statusCode());
            System.out.println("Response headers: " + response.headers());
            response.body().forEach(it -> System.out.println(it));
        }
    }

    public void async() throws InterruptedException {
        HttpClient client = createClient();
        HttpRequest request = createRequest();
        CompletableFuture<Void> future = client.sendAsync(request,
                HttpResponse.BodyHandlers.ofLines())
                .thenAccept(response -> {
                    System.out.println("Response status code: " + response.statusCode());
                    System.out.println("Response headers: " + response.headers());
                });

        while (!future.isDone()) {
            Thread.sleep(1000);
        }

        future.join();
    }

    private HttpRequest createRequest() {
        return HttpRequest.newBuilder()
                .uri(URI.create("http://openjdk.java.net/"))
                .build();
    }

    private HttpClient createClient() {
        return HttpClient.newHttpClient();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        HttpClientDemo demo = new HttpClientDemo();

        demo.async();
        demo.sync();
    }
}
