package com.steeplesoft.jug.java11.demo;

import java.util.ArrayList;

public class VarDemo {
    public void assignment() {
        var foo = new ArrayList<String>();

        foo.add("one");
        foo.add("two");
        foo.add("three");

        foo.forEach(System.out::println);
    }

    public static void main(String[] args) {
        var demo = new VarDemo();

        demo.assignment();
    }
}
