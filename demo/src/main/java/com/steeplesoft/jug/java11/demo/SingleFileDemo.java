package com.steeplesoft.jug.java11.demo;

import java.util.Arrays;

public class SingleFileDemo {
    public static void main(String[] args) {
        Arrays.stream(args).map(Integer::parseInt)
                .forEach(it -> System.out.println (it + "! = " + factorial(it)));
    }

    private static int factorial(int number) {
        return number == 0 ? 1 : number * factorial(number-1);
    }
}
