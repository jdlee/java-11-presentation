package com.steeplesoft.jug.java11.demo;

import java.nio.file.Path;

public class ApiDemo {

    public static void main(String[] args) {

        String sys = Character.toString(49152);
        System.out.println(CharSequence.compare("123", "123") == 0 ? "same" : "different");
        System.out.println(CharSequence.compare("123", "1234") == 0 ? "same" : "different");
        System.out.println(CharSequence.compare("abc", "123") == 0 ? "same" : "different");
        System.out.println("*".repeat(5));
        System.out.println("".isBlank());
        System.out.println(" ".isBlank());
        final String foo = "    Hello, World    ";
        System.out.println("'" + foo.strip() + "'");
        System.out.println("'" + foo.stripTrailing() + "'");
        System.out.println("'" + foo.stripLeading() + "'");

        "one\ntwo\nthree".lines().forEach(System.out::println);

        Path path = Path.of(System.getProperty("user.home"));
        System.out.println(path.toAbsolutePath());

    }
}
